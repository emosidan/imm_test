<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <div class="container">
        <p>Today is <span class="dayNow"></span></p>
        <ul class="day-wrap">
        </ul>
        <p>Selected day is <span class="daySelect"></span></p>
    
        </div>
    </div>
    <script src="app.js"></script>
</body>
</html>