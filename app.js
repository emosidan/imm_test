let dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ,'Sunday'];
const dayNow = document.querySelector('.dayNow');
const daySelect = document.querySelector('.daySelect');
const dayWrap = document.querySelector('.day-wrap');

loadAllTasks();

function loadAllTasks(){
    // Showing all Day
    for(let i=0; i<dayNames.length; i++){
        addDayNames(dayNames[i], i+1);
    }

    // outline change when clicked
    dayWrap.addEventListener('click', dayClicked);
}

// add day Names
function addDayNames(day, numDay){
    let dayss = new Date();
    let days = dayss.getDay() || 7;
    const liDay = document.createElement('li');
    if(numDay == days ){
        liDay.className = 'day-item current'
        dayNow.appendChild(document.createTextNode(day));
        daySelect.appendChild(document.createTextNode(day));
    }else{
        liDay.className = 'day-item';
    }
    liDay.appendChild(document.createTextNode(day));
    dayWrap.appendChild(liDay);
}

// add shadow and outline when clicked
function dayClicked(e){
    const dayClick = e.target;
    document.querySelectorAll('.active').forEach(function(dayAct){
        dayAct.classList.remove('active');
    });
    
    if(dayClick.classList.contains('day-item')){
        dayClick.classList.add('active');
        let dayUpdate = document.createTextNode(dayClick.innerText);
        daySelect.innerText = dayUpdate.nodeValue;
    }
}
